'''
Created on Jun 19, 2018

@author: vkarnati
'''

import pandas as pd
import MySQLdb as mysql
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from sklearn import preprocessing, cross_validation, svm
from sklearn.linear_model import LinearRegression
from matplotlib import style
style.use('fivethirtyeight')
#style.use('ggplot')


#mysql_cn= mysql.connect(host='localhost', port=3306, user='root', passwd='root', db='MY_CRYPTO_DB')
mysql_cn= mysql.connect(host='5.44.105.105', port=3306, user='CRYPTO_APP_USR', passwd='CRYPTO_APP_USR', db='MY_CRYPTO_DB')
df = pd.read_sql("SELECT SOURCE, ASSET_PRICE, date_format(CREATED_TS,'%Y-%m-%d %H:%i:00') AS CREATED_TS from ASSET_EVENT WHERE ASSET_TYPE = 'CURRENCY' ORDER BY ID ASC", con=mysql_cn)

df.set_index('CREATED_TS', inplace=True)

remitlydf = df[df['SOURCE'].str.contains("REMITLY_ECONOMY")]

livedf = df[df['SOURCE'].str.contains("LIVE_USD_TO_INR_PRICE")] 

remitlydf = remitlydf[~remitlydf.index.duplicated()]
livedf = livedf[~livedf.index.duplicated()]

merged_df = pd.merge(remitlydf, livedf, on='CREATED_TS')
merged_df.drop(['SOURCE_x', 'SOURCE_y'], inplace=True, axis=1)
merged_df = merged_df.round(2)
merged_df.dropna(inplace=True)
merged_df.columns = ['REMITLY_PRICE', 'LIVE_USD_TO_INR_PRICE']

#print merged_df.head()

X = np.array(merged_df.drop(['REMITLY_PRICE'], 1))
y = np.array(merged_df['REMITLY_PRICE'])

X_train, X_test, y_train, y_test = cross_validation.train_test_split(X, y, test_size=0.2)
clf = LinearRegression(n_jobs=-1)
clf.fit(X_train, y_train)
confidence = clf.score(X_test, y_test)

forecast_set = clf.predict(X[-5:])

print forecast_set, y[-5:]

#COMMENTED OUT:
#clf = svm.SVR(kernel='linear')
#clf.fit(X_train, y_train)
#confidence = clf.score(X_test, y_test)

#merged_df.plot()
#plt.show()

mysql_cn.close()
