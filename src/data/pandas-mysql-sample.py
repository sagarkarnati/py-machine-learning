'''
Created on Jun 19, 2018

@author: vkarnati
'''

import pandas as pd
import MySQLdb as mysql

import matplotlib.pyplot as plt
from matplotlib import style
style.use('fivethirtyeight')


mysql_cn= mysql.connect(host='localhost', port=3306, user='root', passwd='root', db='MY_CRYPTO_DB')
df = pd.read_sql("select * from ASSET_EVENT WHERE SOURCE = 'COINBASE'", con=mysql_cn)

df = df[['FROM_ASSET_NAME', 'ASSET_PRICE', 'CREATED_TS']]

df.set_index('CREATED_TS', inplace=True)

btcdf = df[df['FROM_ASSET_NAME'].str.contains("BTC")]
ltcdf = df[df['FROM_ASSET_NAME'].str.contains("LTC")]
ethdf = df[df['FROM_ASSET_NAME'].str.contains("ETH")] 

merged_df = pd.concat([btcdf['ASSET_PRICE'], ltcdf['ASSET_PRICE'], ethdf['ASSET_PRICE']], axis=1)
merged_df.columns = ['BTC_PRICE', 'LTC_PRICE', 'ETH_PRICE']

merged_df.dropna(inplace = True)

merged_df.plot()
plt.show()

mysql_cn.close()
